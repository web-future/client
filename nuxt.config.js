export default {
  server: {
    port: process.env.APP_PORT || 3001,
    // host: '0.0.0.0',
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Future - Tìm kiếm việc làm nhanh, tin cậy',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#007456' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/tailwind.scss',
    '@/assets/scss/main.scss',
    'element-ui/lib/theme-chalk/index.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/components.js',
    '@/plugins/mixins.js',
    '@/plugins/axios.js',
    {
      src: '@/plugins/element-ui.js',
      ssr: true,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  env: {
    BASE_URL: process.env.BASE_URL || 'http://localhost:3001',
    APP_PORT: process.env.APP_PORT || 3001,
    API_URL: process.env.API_URL || 'http://localhost:8080/api',
    API_PROVINCE_URL:
      process.env.API_PROVINCE_URL || 'https://vapi.vnappmob.com/api',
    GOOGLE_CLIENT_ID:
      process.env.GOOGLE_CLIENT_ID ||
      '128818098136-2qh7rk8djle6m9vps6a5ksrv0va9g1j3.apps.googleusercontent.com',
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    'vue-ssr-carousel/nuxt',
    // https://go.nuxtjs.dev/tailwindcss
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/firebase',
    'nuxt-socket-io',
  ],

  io: {
    // module options
    sockets: [
      {
        name: 'common',
        url: 'http://localhost:8080',
      },
    ],
  },

  firebase: {
    lazy: false,
    config: {
      apiKey: 'AIzaSyCXQspl9KNb30G3VQyuRCkcC1gLYoMYQuk',
      authDomain: 'future-252a8.firebaseapp.com',
      projectId: 'future-252a8',
      storageBucket: 'future-252a8.appspot.com',
      messagingSenderId: '986727611404',
      appId: '1:986727611404:web:6c447de258f547ec304229',
    },
    onFirebaseHosting: false,
    services: {
      storage: {
        emulatorHost: 'https://firebasestorage.googleapis.com',
      },
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_URL || 'http://localhost:8080/api',
  },
  router: {
    middleware: ['auth'],
  },

  auth: {
    localStorage: false,
    strategies: {
      local: {
        // scheme: 'refresh',
        token: {
          property: 'accessToken',
          global: true,
          type: 'Bearer',
          maxAge: 60 * 60 * 24, // 24h
        },
        // refreshToken: {
        //   property: 'refreshToken',
        //   type: 'Bearer',
        //   maxAge: 60 * 60 * 24 * 2,
        // },
        user: {
          property: '',
        },
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            propertyName: 'data.accessToken',
          },
          // refresh: {
          //   url: 'auth/refresh-token',
          //   method: 'get',
          //   propertyName: 'data.accessToken',
          // },
          user: { url: 'auth/me', method: 'get', propertyName: '' },
          logout: { url: 'auth/logout', method: 'get', propertyName: '' },
        },
      },
      google: {
        clientId:
          '128818098136-2qh7rk8djle6m9vps6a5ksrv0va9g1j3.apps.googleusercontent.com',
        codeChallengeMethod: '',
        responseType: 'id_token token',
      },
    },
    redirect: {
      login: '/dang-nhap',
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
}
