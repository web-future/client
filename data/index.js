import { routesUrl } from '~/constants/routes'

export const navBarList = [
  {
    title: 'Trang chủ',
    to: routesUrl.HOME,
  },
  {
    title: 'Việc làm',
    to: routesUrl.JOB,
  },
  {
    title: 'Công ty',
    to: routesUrl.COMPANIES,
  },
  { title: 'Bài viết', to: routesUrl.POST },
]

export const footer = {
  company: [
    {
      text: 'Liên hệ',
    },
    {
      text: "FAQ's",
    },
  ],
  service: [
    {
      text: 'Việc làm',
    },
  ],
  policies: [
    {
      text: 'Chính sách bảo mật',
    },
    {
      text: 'Điều khoản sử dụng',
    },
  ],
  connect: [
    {
      icon: 'footer/icon-facebook',
      text: 'Facebook',
    },
    {
      icon: 'footer/icon-insta',
      text: 'Instagram',
    },
  ],
}

export const userOption = [
  {
    icon: 'icon-user',
    title: 'Cá nhân',
    to: routesUrl.PERSONAL,
  },
  {
    icon: 'icon-cv-file',
    title: 'Hồ sơ',
    to: routesUrl.CV,
  },
  {
    icon: 'icon-pen',
    title: 'Viết bài',
    to: routesUrl.POST_CREATE,
  },
  {
    icon: 'icon-owner',
    title: 'Bài của tôi',
    to: routesUrl.POST_ME,
  },
  {
    icon: 'icon-password',
    title: 'Mật khẩu',
    to: routesUrl.PASSWORD,
  },
  {
    icon: 'icon-logout',
    title: 'Đăng xuất',
  },
]

export const status = [
  {
    label: 'Chờ xử lý',
    type: '',
  },
  {
    label: 'Chấp nhận',
    type: 'success',
  },
  {
    label: 'Từ chối',
    type: 'danger',
  },
]
