export const LocalStorageAccessTokenKey = 'accessToken'

export const getAccessToken = () => {
  if (process.client) localStorage.getItem(LocalStorageAccessTokenKey)
}

export const saveAccessToken = (value) => {
  if (process.client) localStorage.setItem(LocalStorageAccessTokenKey, value)
}

export const clearAuthorizationToken = () => {
  if (process.client) localStorage.removeItem(LocalStorageAccessTokenKey)
}
