/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './app.vue',
    './error.vue',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    extend: {
      container: {
        center: true,
        padding: {
          DEFAULT: '1rem',
          sm: '1.5rem',
          lg: '2rem',
          xl: '2rem',
          '2xl': '2rem',
        },
      },
      colors: {
        white: '#fff',
        555: '#555555',
        '0074': '#007456',
        111: '#111111',
        '000': '#000000',
        faf4: '#FAF4EF',
        '007456': '#0074560D',
        e7ee: '#E7EEF2',
        ff3: '#ff3333',
        border: '#dadce0',
        border2: '#eeeeee',
        'category-item': 'rgba(0, 116, 86, .05)',
        999: '#999999',
        'pri-hover': 'rgba(0, 116, 86, 0.18)',
        overlay: 'rgba(17,17,17,0.5)',
      },
      fontSize: {
        '16-25': [
          '1rem',
          {
            lineHeight: '1.563rem',
          },
        ],
        '16-22': [
          '1rem',
          {
            lineHeight: '22px',
          },
        ],
        '24-21': [
          '1.5rem',
          {
            lineHeight: '1.3rem',
          },
        ],
        '18-23': [
          '18px',
          {
            lineHeight: '1.44rem',
          },
        ],
        '20-30': ['20px', { lineHeight: '30px' }],
        '28-21': ['28px', { lineHeight: '1.3rem' }],
        '14-24': ['14px', { lineHeight: '1.5rem' }],
        '14-18': ['14px', { lineHeight: '18px' }],
        '28-34': ['28px', { lineHeight: '34px' }],
        '24-30': ['24px', { lineHeight: '30px' }],
      },
      boxShadow: {
        'job-item-horizontial': '0 0 0 0 rgba(0,0,0,.01)',
        'job-item-hover': '0 5px 12px 2px rgba(0,0,0,.05)',
        default: '0 5px 12px 2px rgba(0,0,0,.05)',
        'input-default': '0 0 15px 0 rgba(0,0,0,.03)',
      },
      backgroundImage: {
        'button-hover': 'linear-gradient(rgba(0,0,0,.2)0 0)',
        'banner-home': "url('/images/banner-home.png')",
        'banner-find-job': "url('/images/banner-find-job.png')",
        'banner-find-company': "url('/images/banner-find-company.png')",
        'banner-default': "url('/images/banner-default.png')",
      },
    },
  },
  plugins: [],
}
