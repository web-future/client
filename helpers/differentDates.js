export const differentDates = (dateOne, dateTwo) => {
  const date1 = new Date(dateOne)
  const date2 = new Date(dateTwo)

  const differenceInTime = date2.getTime() - date1.getTime()

  const differenceInDays = Math.round(differenceInTime / (1000 * 3600 * 24))

  return differenceInDays
}
