import Hashids from 'hashids'
import slugify from 'slugify'

export function encodeHash(id) {
  const hashids = new Hashids('Future')
  return hashids.encode(id)
}

export function decodeHash(code) {
  const hashids = new Hashids('Future')
  try {
    const decodeIds = hashids.decode(code)
    if (!(decodeIds && decodeIds.length > 0)) {
      return 0
    }
    return decodeIds[0]
  } catch (e) {
    return 0
  }
}
export function createSlug(str) {
  if (str) {
    str = str.replace(/<[^>]*>/g, '')
    return slugify(str, {
      replacement: '-',
      remove: /[*+~.()'"!:@]/g,
      lower: true,
      strict: true,
      locale: 'vi',
      trim: true,
    })
  }
  return ''
}
export function getContentId(slug) {
  if (slug) {
    return slug.substr(slug.lastIndexOf('-') + 1)
  }
  return 0
}
