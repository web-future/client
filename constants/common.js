export const regexEmail = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/

export const DEFAULT_DATE_FORMAT = 'DD/MM/YYYY'
export const DEFAULT_TIME_FORMAT = 'HH:mm DD/MM/YYYY'
