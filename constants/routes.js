export const routesUrl = {
  HOME: '/',
  JOB: '/viec-lam',
  COMPANIES: '/cong-ty',
  POST: '/bai-viet',
  POST_CREATE: '/bai-viet/tao-moi',
  POST_ME: '/bai-viet/ca-nhan',
  SIGN_IN: '/dang-nhap',
  SIGN_UP: '/dang-ky',
  DASHBOARD: '/thong-ke',
  PERSONAL: '/ca-nhan',
  CV: '/ho-so',
  PASSWORD: '/mat-khau',
  CONVERSATION: '/hoi-thoai',
}

export const dynamicRoutes = {
  CATEGORIES_DETAIL: 'danh-muc-slug',
  JOB_DETAIL: 'danh-muc-slug-jobSlug',
  COMPANY_DETAIL: 'cong-ty-slug',
  POST_DETAIL: 'bai-viet-slug',
  CONVERSATION_DETAIL: 'hoi-thoai-slug',
}
