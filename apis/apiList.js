export const apiList = {
  CATEGORIES: {
    DEFAULT: 'categories',
    DETAIL: 'categories/:id',
  },
  JOBS: {
    DEFAULT: 'jobs',
    DETAIL: 'jobs/:id',
    BY_CATE: 'jobs/:id/cate',
    CV_SENDED: 'jobs/cv/sended',
  },
  COMPANIES: {
    DEFAULT: 'companies',
    DETAIL: 'companies/:id',
  },
  RESUMES: {
    DEFAULT: 'resumes',
    SENDED: 'resumes/sended',
  },
  USERS: {
    DEFAULT: 'users',
    DETAIL: 'users/:id',
    PW_CHANGE: 'users/:email/pw-change',
  },
  FILES: {
    UPLOAD: 'files/upload',
  },
  NOTIFICATIONS: {
    DEFAULT: 'notifications',
    DETAIL: 'notifications/:id',
  },
  POSTS: {
    DEFAULT: 'posts',
    DETAIL: 'posts/:id',
    ME: 'posts/me',
  },
  CHATS: {
    DEFAULT: 'chats',
    ME: 'chats/me',
    DETAIL: 'chats/:id',
    SEEN: 'chats/:id/seen',
  },
  MESSAGES: {
    DEFAULT: 'messages',
    DETAIL: 'messages/:id',
    BY_CHAT: 'messages/:chatId/chat',
  },
}
