export default {
  async nuxtServerInit({ commit }, { app }) {
    if (this.$auth.user) {
      // Get all notification
      const respNoti = await app.context.$customAxios.$get(
        `/notifications/${this.$auth.user.id}/users`
      )
      if (respNoti) commit('notis/SET_NOTI_LIST', respNoti.data)
    }
  },
  changeDevice({ commit }, payload) {
    commit('CHANGE_DEVICE', payload)
  },
  signIn({ commit }, payload) {
    commit('SIGN_IN', payload)
  },
}
