export default {
  changeName({ commit }, payload) {
    commit('CHANGE_NAME', payload)
  },
  changeProvince({ commit }, payload) {
    commit('CHANGE_PROVINCE', payload)
  },
}
