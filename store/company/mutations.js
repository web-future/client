export default {
  CHANGE_NAME(state, payload) {
    state.name = payload
  },
  CHANGE_PROVINCE(state, payload) {
    state.province = payload
  },
}
