export default {
  setMessList({ commit }, payload) {
    commit('SET_MESS_LIST', payload)
  },
  addMess({ commit }, payload) {
    commit('ADD_MESS', payload)
  },
  updateMessList({ commit }, payload) {
    commit('UPDATE_MESS_LIST', payload)
  },
  setChatList({ commit }, payload) {
    commit('SET_CHAT_LIST', payload)
  },
  updateChatList({ commit }, payload) {
    commit('UPDATE_CHAT_LIST', payload)
  },
  addChat({ commit }, payload) {
    commit('ADD_CHAT', payload)
  },
}
