export default {
  messageList: (state) => state.messageList,
  chatList: (state) => state.chatList,
}
