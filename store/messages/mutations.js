import { find } from 'lodash'

export default {
  SET_MESS_LIST(state, payload) {
    state.messageList = payload
  },
  ADD_MESS(state, payload) {
    if (!find(state.messageList, { id: payload.id })) {
      state.messageList.push(payload)
    }
  },
  UPDATE_MESS_LIST(state, payload) {
    state.messageList = state.messageList.map((mess) => {
      if (+mess.id === +payload.id) {
        return payload
      }
      return mess
    })
  },
  SET_CHAT_LIST(state, payload) {
    state.chatList = payload
  },
  UPDATE_CHAT_LIST(state, payload) {
    state.chatList = state.chatList.map((chat) => {
      if (+chat.id === +payload.id) {
        return {
          ...chat,
          messages: payload.messages,
        }
      }
      return chat
    })
  },
  ADD_CHAT(state, payload) {
    if (!find(state.chatList, { id: payload.id })) {
      state.chatList.push(payload)
    }
  },
}
