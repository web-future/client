export default {
  CHANGE_DEVICE(state, payload) {
    state.isDeviceWeb = payload
  },
  SIGN_IN(state, payload) {
    state.currentAuth = payload
  },
}
