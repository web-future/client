export default {
  isDesktop: (state) => state.isDeviceWeb,
  layoutName: (state) => (state.isDeviceWeb ? 'layout-web' : 'layout-mobile'),
  isAuthenticated: (state) => state.auth.loggedIn,
  loggedInUser: (state) => state.auth.user,
}
