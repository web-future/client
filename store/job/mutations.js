export default {
  CHANGE_SKILL(state, payload) {
    state.skill = payload
  },
  CHANGE_LOCATION(state, payload) {
    state.location = payload
  },
}
