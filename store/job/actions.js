export default {
  changeSkill({ commit }, payload) {
    commit('CHANGE_SKILL', payload)
  },
  changeLocation({ commit }, payload) {
    commit('CHANGE_LOCATION', payload)
  },
}
