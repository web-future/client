export default {
  SET_NOTI_LIST(state, payload) {
    state.notiList = payload
  },
  ADD_NOTI(state, payload) {
    state.notiList.unshift(payload)
  },
  UPDATE_NOTI(state, payload) {
    const noti = state.notiList.find(({ id }) => `${id}` === `${payload.id}`)
    if (noti) {
      const index = state.notiList.indexOf(noti)
      noti.isRead = payload.isRead
      state.notiList[index] = noti
    }
  },
}
