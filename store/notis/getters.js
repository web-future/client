export default {
  notiList: (state) => state.notiList,
  notiCount: (state) => state.notiList.filter(({ isRead }) => !isRead).length,
}
