import dayjs from 'dayjs'
import { DEFAULT_DATE_FORMAT, DEFAULT_TIME_FORMAT } from '@/constants'

export default {
  methods: {
    formatDate(time) {
      return dayjs(time).format(DEFAULT_DATE_FORMAT)
    },
    formatTime(time) {
      return dayjs(time).format(DEFAULT_TIME_FORMAT)
    },
  },
}
