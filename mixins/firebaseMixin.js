export default {
  methods: {
    async uploadFile(file) {
      const storageRef = this.$fire.storage.ref().child(file.name)
      try {
        await storageRef.put(file)
        return await storageRef.getDownloadURL()
      } catch (e) {
        alert(e.message)
      }
    },
  },
}
