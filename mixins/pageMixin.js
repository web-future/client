export default {
  head() {
    return {
      link: [
        {
          rel: 'canonical',
          href: `${process.env.BASE_URL}${this.$route.path}`,
        },
      ],
    }
  },
  methods: {
    headPage(title, description = '', keyword = '', thumb = '') {
      return {
        title,
        description,
        meta: [
          {
            hid: 'title',
            name: 'title',
            content: title,
          },
          {
            hid: 'description',
            name: 'description',
            content: description,
          },
          {
            hid: 'og:type',
            property: 'og:type',
            content: 'website',
          },
          {
            hid: 'og:title',
            name: 'og:title',
            content: title,
          },
          {
            hid: 'og:description',
            name: 'og:description',
            content: description,
          },
          {
            hid: 'keywords',
            name: 'keywords',
            content: keyword,
          },
          {
            hid: 'og:keywords',
            name: 'og:keywords',
            content: keyword,
          },
          {
            hid: 'og:image',
            property: 'og:image',
            content: thumb,
          },
          {
            hid: 'og:image:width',
            property: 'og:image:width',
            content: '1200',
          },
          {
            hid: 'og:image:height',
            property: 'og:image:height',
            content: '630',
          },
        ],
        link: [
          {
            rel: 'canonical',
            href: `${process.env.BASE_URL}/${this.$route.path}`,
          },
        ],
      }
    },
  },
}
