import { regexEmail } from '~/constants'

export default {
  data() {
    return {
      formData: {
        fullname: this.$store.getters.loggedInUser
          ? this.$store.getters.loggedInUser.fullname
          : '',
        email: '',
        password: '',
        currentPw: '',
        newPw: '',
        confirmPw: '',
        phoneNumber: this.$store.getters.loggedInUser
          ? this.$store.getters.loggedInUser.phoneNumber
          : '',
      },
      file: {},
      startValidate: false,
      errorMessage: '',
    }
  },
  methods: {
    isRequired(payload) {
      return this.startValidate && !payload.trim()
    },
    isValidEmail() {
      return (
        this.startValidate &&
        this.formData.email.trim() &&
        !regexEmail.test(this.formData.email)
      )
    },
    isMatchPw() {
      return (
        this.startValidate &&
        this.formData.newPw.trim() &&
        this.formData.confirmPw.trim() &&
        !(this.formData.newPw.trim() === this.formData.confirmPw.trim())
      )
    },
    clearData() {
      this.formData.fullname = ''
      this.formData.email = ''
      this.formData.password = ''
      this.formData.currentPw = ''
      this.formData.newPw = ''
      this.formData.confirmPw = ''
      this.formData.phoneNumber = ''
      this.file = {}
      this.startValidate = false
      this.errorMessage = ''
    },
  },
}
