import { provinceData } from '~/data/province'
export default {
  data() {
    return {
      options: [],
    }
  },
  async fetch() {
    const data = await fetch(`${process.env.API_PROVINCE_URL}/province/`)
      .then((res) => res.json())
      .then((res) => res.results)

    if (data) {
      this.options = data
    } else {
      this.options = provinceData
    }
  },
}
