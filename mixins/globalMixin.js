import { mapGetters, mapActions } from 'vuex'
import { differentDates } from '~/helpers/differentDates'

export default {
  computed: {
    ...mapGetters({
      isDesktop: 'isDesktop',
      loggedInUser: 'loggedInUser',
      isAuthenticated: 'isAuthenticated',
    }),
    diffDateCount() {
      return differentDates(new Date(), this.job.endDate)
    },
  },
  methods: {
    ...mapActions({ changeDevice: 'changeDevice', signIn: 'signIn' }),
    level(number) {
      switch (number) {
        case 0:
          return 'Không yêu cầu'
        case 1:
          return 'Fresher'
        case 2:
          return 'Fresher'
        case 3:
          return 'Middle'
        case 4:
          return 'Senior'
        default:
          return 'TechLead'
      }
    },
    partnerConversation(members) {
      return members.find((member) => +member.id !== +this.loggedInUser.id)
    },
  },
}
