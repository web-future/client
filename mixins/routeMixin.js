import { dynamicRoutes } from '@/constants'
import { createSlugHash, createSlug, encodeHash } from '~/helpers'

export default {
  methods: {
    routeCateDetail({ id, name }) {
      return {
        name: dynamicRoutes.CATEGORIES_DETAIL,
        params: {
          slug: createSlugHash(id, name),
        },
      }
    },
    routeJobDetail({ cateLabel, id, jobName }) {
      return {
        name: dynamicRoutes.JOB_DETAIL,
        params: {
          slug: createSlug(cateLabel),
          jobSlug: createSlugHash(id, jobName),
        },
      }
    },
    routeCompanyDetail({ id, name }) {
      return {
        name: dynamicRoutes.COMPANY_DETAIL,
        params: {
          slug: createSlugHash(id, name),
        },
      }
    },
    routePostDetail({ id, title }) {
      return {
        name: dynamicRoutes.POST_DETAIL,
        params: {
          slug: createSlugHash(id, title),
        },
      }
    },
    routeConversationDetail({ chatId }) {
      return {
        name: dynamicRoutes.CONVERSATION_DETAIL,
        params: {
          slug: `${encodeHash(chatId)}`,
        },
      }
    },
  },
}
