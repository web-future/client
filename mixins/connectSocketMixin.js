import PusherJs from 'pusher-js'

export default {
  data() {
    return {
      pusher: null,
    }
  },
  mounted() {
    PusherJs.logToConsole = true
    this.pusher = new PusherJs('fbfb409494432654e2fa', {
      cluster: 'ap1',
    })
  },
}
