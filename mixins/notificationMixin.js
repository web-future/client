import { mapActions, mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters('notis', {
      notiCount: 'notiCount',
      notiList: 'notiList',
    }),
  },
  mounted() {
    if (this.$store.getters.loggedInUser) {
      const channel = this.pusher.subscribe(
        `${this.$store.getters.loggedInUser.id}`
      )
      channel.bind('getNoti', (resp) => {
        this.addNoti({
          id: resp.id,
          sender: resp.sender,
          content: resp.status,
          createdAt: resp.createdAt,
        })
      })
    }
  },
  methods: {
    ...mapActions('notis', {
      addNoti: 'addNoti',
    }),
  },
}
