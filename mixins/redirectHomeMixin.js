export default {
  created() {
    if (this.authorization) {
      return this.$router.back()
    }
  },
}
