import { mapActions, mapGetters } from 'vuex'
import { decodeHash } from '~/helpers'
import { apiList } from '~/apis/apiList'

export default {
  computed: {
    ...mapGetters('messages', {
      messageList: 'messageList',
      chatList: 'chatList',
    }),
  },
  mounted() {
    if (this.$store.getters.loggedInUser) {
      const chatIdEncode = this.$route.params.slug
      const chatId = decodeHash(chatIdEncode)
      const channel = this.pusher.subscribe(`${chatId}`)
      const channel2 = this.pusher.subscribe(
        `${this.$store.getters.loggedInUser.email}`
      )

      channel.bind('messages:new', (resp) => {
        console.log('messages:new', resp)
        this.seenMessage()
        this.addMess(resp)
      })
      channel.bind('messages:update', (resp) => {
        this.updateMessList(resp)
      })

      channel2.bind('chat:update', (resp) => {
        console.log('chat:update', resp)
        this.updateChatList(resp)
      })

      channel2.bind('chat:new', (resp) => {
        this.addChat(resp)
      })
    }
  },
  methods: {
    ...mapActions('messages', {
      addMess: 'addMess',
      updateMessList: 'updateMessList',
      updateChatList: 'updateChatList',
      addChat: 'addChat',
    }),
    seenMessage() {
      const chatIdEncode = this.$route.params.slug
      const chatId = decodeHash(chatIdEncode)
      return this.$customAxios
        .$post(apiList.CHATS.SEEN.replace(':id', chatId))
        .then((res) => res)
        .catch((error) => console.log(error))
    },
  },
  unmounted() {
    const chatIdEncode = this.$route.params.slug
    const chatId = decodeHash(chatIdEncode)
    const channel = this.pusher.unsubscribe(`${chatId}`)
    const channel2 = this.pusher.unsubscribe(
      `${this.$store.getters.loggedInUser.email}`
    )

    channel.unbind('messages:new', (resp) => {
      this.seenMessage()
      this.addMess(resp)
    })
    channel.unbind('messages:update', (resp) => {
      this.updateMessList(resp)
    })
    channel2.unbind('chat:update', (resp) => {
      this.updateChatList(resp)
    })
    channel2.unbind('chat:new', (resp) => {
      this.addChat(resp)
    })
  },
}
