import Vue from 'vue'

import StaticImage from '@/components/common/images/StaticImage.vue'
import StaticSvg from '@/components/common/images/StaticSvg.vue'
import ButtonPrimary from '@/components/common/buttons/ButtonPrimary.vue'
import ButtonLink from '@/components/common/buttons/ButtonLink.vue'
import ButtonDisable from '@/components/common/buttons/ButtonDisable.vue'
import TextBase from '@/components/common/texts/TextBase.vue'
import TextTitleBase from '@/components/common/texts/TextTitleBase.vue'
import JobItemHorizontial from '@/components/common/items/JobItemHorizontial.vue'
import CompanyHorizontial from '@/components/common/items/CompanyHorizontial.vue'
import SkeletonItem from '@/components/common/items/SkeletonItem.vue'
import ButtonArrowSlide from '@/components/common/buttons/ButtonArrowSlide.vue'
import Breadcrumb from '@/components/common/items/Breadcrumb.vue'
import TagInfo from '@/components/common/items/TagInfo.vue'
import PostItem from '@/components/common/items/PostItem.vue'
import ConversationItem from '@/components/common/items/ConversationItem.vue'

const components = {
  StaticImage,
  StaticSvg,
  ButtonPrimary,
  ButtonLink,
  TextBase,
  TextTitleBase,
  JobItemHorizontial,
  SkeletonItem,
  ButtonArrowSlide,
  Breadcrumb,
  TagInfo,
  ButtonDisable,
  CompanyHorizontial,
  PostItem,
  ConversationItem,
}

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
