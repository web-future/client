export default function ({ $axios, app }, inject) {
  const options = {
    baseURL: `${process.env.API_URL}`,
    headers: {
      Accept: 'application/json',
    },
    credentials: true,
  }
  const fetcher = $axios.create(options)
  fetcher.interceptors.request.use(
    (config) => {
      // config.headers.Authorization = `Bearer ${
      //   req.headers.cookie.split('; ')[3].split('=')[1].split('%20')[1]
      // }`
      // config.headers.Accept = 'application/json'

      if (app.$auth.loggedIn) {
        const token = app.$auth.strategy.token.get().split(' ')[1]
        fetcher.setToken(token, 'Bearer')
      }

      return config
    },
    (requestError) => {
      // eslint-disable-next-line no-undef
      Raven.captureException(requestError)

      return Promise.reject(requestError)
    }
  )
  fetcher.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response && error.response.status >= 500) {
        // eslint-disable-next-line no-undef
        Raven.captureException(error)
      }

      return Promise.reject(error)
    }
  )

  inject('customAxios', fetcher)
}
