import Vue from 'vue'
import globalMixin from '@/mixins/globalMixin'
import routeMixin from '@/mixins/routeMixin'

Vue.mixin(globalMixin)
Vue.mixin(routeMixin)
