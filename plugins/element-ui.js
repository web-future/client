import Vue from 'vue'

import {
  Popover,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Dialog,
  Button,
  Input,
  Slider,
  Loading,
  Select,
  Option,
  Notification,
  DatePicker,
  Drawer,
  Progress,
  Tooltip,
  Pagination,
  Skeleton,
  SkeletonItem,
  MessageBox,
  CollapseTransition,
  Message,
  Timeline,
  TimelineItem,
  Collapse,
  CollapseItem,
  Form,
  Divider,
  Tag,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Empty,
  Upload,
  Table,
  TableColumn,
  Badge,
} from 'element-ui'

import lang from 'element-ui/lib/locale/lang/vi'
import locale from 'element-ui/lib/locale'
// configure language
locale.use(lang)
Vue.component(Popover.name, Popover)
Vue.component(Dropdown.name, Dropdown)
Vue.component(DropdownMenu.name, DropdownMenu)
Vue.component(DropdownItem.name, DropdownItem)
Vue.component(Skeleton.name, Skeleton)
Vue.component(SkeletonItem.name, SkeletonItem)
Vue.component(Dialog.name, Dialog)
Vue.component(Button.name, Button)
Vue.component(Select.name, Select)
Vue.component(Option.name, Option)
Vue.component(Notification.name, Notification)
Vue.component(Input.name, Input)
Vue.component(Slider.name, Slider)
Vue.component(DatePicker.name, DatePicker)
Vue.component(Drawer.name, Drawer)
Vue.component(Progress.name, Progress)
Vue.component(Tooltip.name, Tooltip)
Vue.component(Pagination.name, Pagination)
Vue.component(Form.name, Form)
Vue.component(CollapseTransition.name, CollapseTransition)
Vue.component(Timeline.name, Timeline)
Vue.component(TimelineItem.name, TimelineItem)
Vue.component(Collapse.name, Collapse)
Vue.component(CollapseItem.name, CollapseItem)
Vue.component(Divider.name, Divider)
Vue.component(Tag.name, Tag)
Vue.component(Menu.name, Menu)
Vue.component(MenuItem.name, MenuItem)
Vue.component(MenuItemGroup.name, MenuItemGroup)
Vue.component(Submenu.name, Submenu)
Vue.component(Empty.name, Empty)
Vue.component(Upload.name, Upload)
Vue.component(Table.name, Table)
Vue.component(TableColumn.name, TableColumn)
Vue.component(Badge.name, Badge)
Vue.use(Loading.directive)
Vue.prototype.$loading = Loading.service
Vue.prototype.$notify = Notification
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$message = Message
